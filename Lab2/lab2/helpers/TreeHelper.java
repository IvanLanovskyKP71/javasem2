package helpers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import FS.Directory;
import FS.File;

import java.util.ArrayList;
import java.util.concurrent.RecursiveTask;

public class TreeHelper extends RecursiveTask<JsonNode> {
    private final Directory directory;
    ObjectMapper mapper = new ObjectMapper();

    public TreeHelper(Directory directory) {
        this.directory = directory;
    }

    @Override
    protected JsonNode compute() {
        ArrayNode root = mapper.createArrayNode();
        ArrayList<TreeHelper> subHelpers = new ArrayList<>();

        ArrayList<? super File> children = directory.getFiles();
        for (Object object : children) {
            File child = (File) object;

            if (child.getType() == "Directory") {
                TreeHelper helper = new TreeHelper((Directory) child);
                helper.fork();
                subHelpers.add(helper);
            }
            else {
                JsonNode node = mapper.createObjectNode().put(child.getName(), child.getType());
                root.add(node);
            }
        }

        for (TreeHelper helper : subHelpers) {
            JsonNode jsonNode = helper.join();
            root.add(jsonNode);
        }

        return mapper.createObjectNode().put(directory.getName(), root);
    }
}