package helpers;

import FS.Directory;
import FS.File;

import java.util.ArrayList;
import java.util.concurrent.RecursiveTask;

public class SearchHelper extends RecursiveTask<ArrayList<String>> {
    private final Directory directory;
    private final String pattern;

    public SearchHelper(Directory directory, String pattern) {
        this.directory = directory;
        this.pattern = pattern;
    }

    @Override
    protected ArrayList<String> compute() {
        ArrayList<String> path = new ArrayList<>();
        ArrayList<SearchHelper> subHelpers = new ArrayList<>();

        ArrayList<? super File> children = directory.getFiles();
        for (Object object : children) {
            File child = (File) object;

            if(child.getName().contains(pattern))
                path.add(child.getPath());
            if (child.getType() == "Directory") {
                SearchHelper helper = new SearchHelper((Directory) child, pattern);
                helper.fork();
                subHelpers.add(helper);
            }
        }

        for (SearchHelper helper : subHelpers) {
            path.addAll(helper.join());
        }

        return path;
    }
}
