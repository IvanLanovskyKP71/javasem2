package FS;

import java.util.concurrent.ConcurrentLinkedQueue;

public class BufferFile<T> extends File {
    private ConcurrentLinkedQueue<T> queue; // thread-safe queue
    private static final int MAX_BUF_FILE_SIZE = 20;

    public BufferFile(String name, Directory parent) {
        super("BufferFile", name, parent);
        queue = new ConcurrentLinkedQueue<>();
        parent.addFile(this);
    }

    public static <T> BufferFile<T> create(String name, Directory parent) {
        if(name.equals("") || name.equals("/") || parent == null || parent.full()) return null;

        return new BufferFile<>(name, parent);
    }

    public T consume() {
        return queue.poll();
    }

    public boolean push(T data) {
        if(queue.size() == MAX_BUF_FILE_SIZE) return false;

        queue.add(data);
        return true;
    }

    public boolean full() {
        return queue.size() == MAX_BUF_FILE_SIZE;
    }

    public boolean isEmpty() {
        return queue.size() == 0;
    }
}
