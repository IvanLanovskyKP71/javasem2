package FS;

import java.util.concurrent.atomic.AtomicReference;

public class MyFile extends File {
    private AtomicReference<String> data;

    public MyFile(String name, Directory parent, String data) {
        super("MyFile", name, parent);
        this.data = new AtomicReference<>(data);
        parent.addFile(this);
    }

    public static MyFile create(String name, Directory parent, String data) {
        if(name.equals("") || name.equals("/") || parent == null || parent.full()) return null;

        return new MyFile(name, parent, data);
    }

    public String getNum() {
        return data.get();
    }

    public synchronized boolean add(String data) {
        int result = Integer.parseInt(data) + Integer.parseInt(getNum());
        this.data.set(Integer.toString(result));
        return this.data.get().equals(Integer.toString(result));
    }
}
