package actions;

import FS.Directory;

public class Full implements FSAction<Boolean> {
    private final Directory dir;

    public Full(Directory dir) {
        this.dir = dir;
    }

    @Override
    public Boolean execute() {
        return dir.full();
    }
}
