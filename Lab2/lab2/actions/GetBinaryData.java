package actions;

import FS.BinaryFile;

public class GetBinaryData implements FSAction<String> {
    private final BinaryFile bin;

    public GetBinaryData(BinaryFile bin) {
        this.bin = bin;
    }

    @Override
    public String execute() {
        return bin.getData();
    }
}
