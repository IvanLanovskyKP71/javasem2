package actions;

import FS.Directory;
import FS.File;

public class RemoveFile implements FSAction<Boolean> {
    private final Directory dir;
    private final File file;

    public RemoveFile(Directory dir, File file) {
        this.dir = dir;
        this.file = file;
    }

    @Override
    public Boolean execute() {
        return dir.removeFile(file);
    }
}
