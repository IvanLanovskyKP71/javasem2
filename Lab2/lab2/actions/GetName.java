package actions;

import FS.File;

public class GetName implements FSAction<String> {
    private final File file;

    public GetName(File file) {
        this.file = file;
    }

    @Override
    public String execute() {
        return file.getName();
    }
}
