package actions;

import FS.File;

public class GetType implements FSAction<String> {
    private final File file;

    public GetType(File file) {
        this.file = file;
    }

    @Override
    public String execute() {
        return file.getType();
    }
}
