package actions;

import java.io.IOException;

public interface FSAction<T> {
    T execute() throws IOException;
}
