package actions;

import FS.Directory;
import FS.File;

public class GetParent implements FSAction<Directory> {
    private final File file;

    public GetParent(File file) {
        this.file = file;
    }

    @Override
    public Directory execute() {
        return file.getParent();
    }
}
