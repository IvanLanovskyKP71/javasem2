package tasks;

import FS.*;
import actions.*;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.ExecutionException;

public class Generator {
    Controller controller;

    public Generator() {
        controller = new Controller(10);
    }

    public Boolean doStressTest(Integer iterations) throws IOException, ExecutionException, InterruptedException {
        Directory root = Directory.create("root", null);
        Directory subDir = Directory.create("subdir", root);
        BinaryFile bin = BinaryFile.create("bin", root, "something");
        BufferFile buff = BufferFile.<String>create("buff", subDir);
        LogFile log = LogFile.create("log", root, "data");
        MyFile myFile = MyFile.create("myFile", subDir, "165");

        Random random = new Random();

        int min = 11;
        int max = 1000;
        int diff = max - min;


        for(int i = 0; i < iterations;) {
            int randomInt = random.nextInt(max) + min;

            if (randomInt % 11 == 0) {
                i++;
                if(!controller.submitFSAction(new Add(myFile, "7845")).get()) {
                    return false;
                }
                continue;
            }
            if (randomInt % 12 == 0) {
                i++;
                if(!controller.submitFSAction(new AddFile(root, new File("Test", "file1", root))).get() && !root.full()) {
                    return false;
                }
                continue;
            }
            if (randomInt % 13 == 0) {
                i++;
                if(!controller.submitFSAction(new Append(log, "Additional log")).get()) {
                    return false;
                }
                continue;
            }
            if (randomInt % 14 == 0) {
                i++;
                if(!controller.submitFSAction(new Push<String>(buff, "some data2")).get() && !buff.full()) {
                    return false;
                }
                continue;
            }
            if (randomInt % 15 == 0) {
                i++;
                if(!buff.isEmpty() && controller.submitFSAction(new Consume<String>(buff)).get() == null) {
                    return false;
                }
                continue;
            }
            if (randomInt % 16 == 0) {
                i++;
                if(controller.submitFSAction(new Count(root, false)).get() != root.count(false)
                        && controller.submitFSAction(new Count(root, true)).get() != root.count(true)) {
                    return false;
                }
                continue;
            }
            if (randomInt % 17 == 0) {
                i++;
                if(!root.full() && !controller.submitFSAction(new Create(root, "testbin" + i, "BinaryFile", "some data for data")).get().getName().equals("testbin" + i)) {
                    return false;
                }
                continue;
            }
            if (randomInt % 19 == 0) {
                i++;
                if(controller.submitFSAction(new Full(root)).get() != root.full()) {
                    return false;
                }
                continue;
            }
            if (randomInt % 20 == 0) {
                i++;
                if(!controller.submitFSAction(new GetBinaryData(bin)).get().equals(bin.getData())) {
                    return false;
                }
                continue;
            }
            if (randomInt % 21 == 0) {
                i++;
                if(!controller.submitFSAction(new GetFiles(root)).get().equals(root.getFiles())) {
                    return false;
                }
                continue;
            }
            if (randomInt % 22 == 0) {
                i++;
                if(!controller.submitFSAction(new GetLogData(log)).get().equals(log.getLogs())) {
                    return false;
                }
                continue;
            }
            if (randomInt % 23 == 0) {
                i++;
                if(!controller.submitFSAction(new GetName(bin)).get().equals(bin.getName())) {
                    return false;
                }
                continue;
            }
            if (randomInt % 24 == 0) {
                i++;
                if(!controller.submitFSAction(new GetNum(myFile)).get().equals(myFile.getNum())) {
                    return false;
                }
                continue;
            }
            if (randomInt % 25 == 0) {
                i++;
                if(!controller.submitFSAction(new GetParent(bin)).get().equals(root)) {
                    return false;
                }
                continue;
            }
            if (randomInt % 26 == 0) {
                i++;
                if(!controller.submitFSAction(new GetType(buff)).get().equals("BufferFile")) {
                    return false;
                }
                continue;
            }
            if (randomInt % 27 == 0) {
                i++;
                if(!controller.submitFSAction(new LongestPath(root)).get().equals(root.longestPath())) {
                    return false;
                }
                continue;
            }
            if (randomInt % 28 == 0) {
                i++;
                BinaryFile binaryFile = BinaryFile.create("test" + i, root, "new data");
                if(!controller.submitFSAction(new MoveTo(binaryFile, subDir)).get() && !subDir.full()) {
                    return false;
                }
                if(!controller.submitFSAction(new RemoveFile(subDir, binaryFile)).get()) {
                    return false;
                }
                continue;
            }
            if (randomInt % 29 == 0) {
                i++;
                if(!controller.submitFSAction(new Search(root, "test")).get().equals(root.search("test"))) {
                    return false;
                }
                continue;
            }
            if (randomInt % 30 == 0) {
                i++;
                if(!controller.submitFSAction(new Tree(root)).get().getClass().equals(String.class)) {
                    return false;
                }
                continue;
            }
        }

        controller.stop();

        return true;
    }
}
