package FS;

public class LogFile extends File {
    private StringBuilder data = new StringBuilder();

    public LogFile(String name, Directory parent, String data) {
        super("LogFile", name, parent);
        this.data.append(data);
        parent.addFile(this);
    }

    public static LogFile create(String name, Directory parent, String data) {
        if(name.equals("") || name.equals("/") || parent == null || parent.full()) return null;

        return new LogFile(name, parent, data);
    }

    public synchronized String getData() {
        return data.toString();
    }

    public synchronized void append(String data) {
        this.data.append(data);
    }
}
