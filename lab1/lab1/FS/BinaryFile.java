package FS;

public class BinaryFile extends File {
    private final String data;

    public BinaryFile(String name, Directory parent, String data) {
    	
        super("BinaryFile", name, parent);
        this.data = data;
        parent.addFile(this);
    }

    public static BinaryFile create(String name, Directory parent, String data) {
        if(name.equals("") || name.equals("/") || parent == null || parent.full()) return null;

        return new BinaryFile(name, parent, data);
    }
    public String getData() {
        return data;
    }
}
