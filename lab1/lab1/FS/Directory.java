package FS;

import java.util.ArrayList;

public class Directory extends File {
    private static final int DIR_MAX_ELEMS = 20;
    private ArrayList<? super File> files;

    private Directory(String name, Directory parent) {
        super("Directory", name, parent);
        files = new ArrayList<>();
        if(parent != null)
            parent.addFile(this);
    }

    public static Directory create(String name, Directory parent) {
        if(name.equals("") || name.equals("/") || (parent != null && parent.full())) return null;

        return new Directory(name, parent);
    }

    public synchronized boolean addFile(File file) {
        if(!files.contains(file) && (file != null && files.size() != DIR_MAX_ELEMS))
            files.add(file);
        else return false;
        return true;
    }

    public synchronized void removeFile(File file) {
        files.remove(file);
    }

    public synchronized ArrayList<? super File> getFiles() {
        return (ArrayList<? super File>)files.clone();
    }

    public synchronized boolean full() {
        return files.size() == DIR_MAX_ELEMS;
    }

    public static void showAllFiles(Directory root, String indent) {
        System.out.println(indent + "└─" + root.getName());
        prettyPrint(root, indent);
    }

    private synchronized int size() {
        return files.size();
    }

    private static void prettyPrint(Directory dir, String indent) {
        if(dir.size() != 0) {
            ArrayList<? super File> files = dir.getFiles();
            int size = files.size();
            for(int i = 0; i < size; i++) {
                File file = (File) files.get(i);
                if(file.getType() != "Directory")
                    if(size - i > 1)
                        System.out.println("  " + indent + "├─" + file.getName());
                    else
                        System.out.println("  " + indent + "└─" + file.getName());
                else
                {
                    if(size - i > 1) {
                        System.out.println("  " + indent + "├─" + file.getName());
                        prettyPrint((Directory) file, indent + "|  ");
                    }
                    else
                    {
                        System.out.println("  " + indent + "└─" + file.getName());
                        prettyPrint((Directory) file, indent + "  ");
                    }
                }
            }
        }
    }
}
