package task;
import FS.*;

public class Task2 {
    public static void main(String[] args) {
        Directory root = Directory.create("root", null);
        Directory dir1 = Directory.create("dir1", root);
        Directory dir2 = Directory.create("dir2", root);
        Directory dir3 = Directory.create("dir3", root);
        
        Directory subDir1 = Directory.create("subDir1", dir2);
        Directory subDir2 = Directory.create("subDir2", dir2);
        Directory subDir3 = Directory.create("subDir3", dir3);
        Directory subSubDir1 = Directory.create("subSubDir1", subDir1);

        BinaryFile binFile1 = BinaryFile.create("BinFile1.bin", dir1, "First BinaryFile");
        BinaryFile binFile2 = BinaryFile.create("BinFile2.bin", dir1, "Second BinaryFile");
        BinaryFile binFile3 = BinaryFile.create("BinFile3.bin", dir3, "Third BinaryFile");
        BinaryFile binFile4 = BinaryFile.create("BinFile4.bin", subDir1, "Fourth BinaryFile");
        BinaryFile binFile5 = BinaryFile.create("BinFile5.bin", subSubDir1, "Fifth BinaryFile");
        BinaryFile binFile6 = BinaryFile.create("BinFile6.bin", subDir3, "Sixth BinaryFile");

        BufferFile buffFile1 = BufferFile.<Integer>create("BuffFile1.buff", dir1);
        BufferFile buffFile2 = BufferFile.<Integer>create("BuffFile2.buff", subDir2);

        LogFile logFile1 = LogFile.create("LogFile1.log", dir3, "This is info of the program log");
        LogFile logFile2 = LogFile.create("LogFile2.log", subSubDir1, "String 2 of the log");

        MyFile file1 = MyFile.create("UserFile1.my", subDir1, "1");
        MyFile file2 = MyFile.create("UserFile2.my", dir1, "2");
        MyFile file3 = MyFile.create("UserFile3.my", subDir3, "3");

        Directory.showAllFiles(root, "");
    }
}
